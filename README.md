# Hi, I'm Jorge Martínez Mamani. 👋

# 🛠 &nbsp;Tech Stack

![C#](https://img.shields.io/badge/C%23-239120?style=flat-square&logo=c-sharp&logoColor=white)
![Unity](https://img.shields.io/badge/Unity-222c37?style=flat-square&logo=unity&logoColor=white)
![Phaser](https://img.shields.io/badge/Phaser-darkviolet?style=flat-square&logo=phaser&logoColor=white)
![Visual Studio Code](https://img.shields.io/badge/Visual%20Studio%20Code-f0efe7?style=flat-square&logo=visual-studio-code&logoColor=blue)

---

<a href="https://github.com/krows-mj" target="_blank">
  
![GitHub](https://img.shields.io/badge/GitHub-222c37?style=flat-square&logo=github&logoColor=white)
</a>
